import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import Contact from "./components/Contact.vue";

Vue.use(VueRouter);
Vue.config.productionTip = false;

const router = new VueRouter({
	mode: "history",
	base: __dirname,
	routes: [{ path: "/existing", Contact }],
});

new Vue({
	router,
	render: (h) => h(App),
}).$mount("#app");
